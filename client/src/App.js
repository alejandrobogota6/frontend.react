import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import TaskForm from "./components/TaskForm/TaskForm";
import TaskList from "./components/TaskList/TaskList";
import Menu from './components/Navbar/Navbar';
import { Container } from "@mui/material";

function App() {
  return (
    <BrowserRouter>
      < Menu/>
      <Container >
        <Routes>
          <Route path="/" element={<TaskList />} />
          <Route path="/tasks/new" element={<TaskForm />} />
          <Route path="/tasks/:id/edit" element={<TaskForm />} />
        </Routes>
      </Container>
    </BrowserRouter>
  );
}

export default App;
