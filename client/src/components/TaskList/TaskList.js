import axios from 'axios';
import React from 'react';
import './TaskList.css'
//Utilizar use Effect para traer la inforamcion al cargar el componente
import { useEffect, useState } from 'react';
import { Card, CardContent, Typography, Button, TextField } from '@mui/material';
import { useNavigate } from 'react-router-dom';

export default function TaskList() {

  const [accounts, setAccounts] = useState([])
  const navigate = useNavigate()
  

  const loadingAccounts = async () => {
    const response = await axios.get('http://localhost:4000/tasks')
    setAccounts(response.data)
  }

  const handleDelete = async (id) => {
    try {
      const response = await axios.delete(`http://localhost:4000/tasks/${id}`)
      setAccounts(accounts.filter(accounts => accounts.user_id !== id));
    } catch (error) {
      console.log(error)
    }
    
  }

  const [ visible, setVisible ] = useState(true);

  const handleChange = async () => {
    
    try {
      setVisible(!visible);
      //const response = await axios.put()
      
    } catch (error) {
      
    }
  }

  useEffect(() => {
    loadingAccounts()
  }, [])

  return (
    <>
    {
      accounts.map((accounts) => (
        <Card className='CardListAccounts'>
          <CardContent className='CardContentList'>
            <div className='DivCardContent'>
              <Typography className='TypographyCard'>{accounts.username}</Typography>
              <Typography className='TypographyCard'>{accounts.password}</Typography>
            </div>
            <div>
            <Button className='ButtonEdit' variant='outlined' onClick={() => navigate(`tasks/${accounts.user_id}/edit`)}>
              Edit
            </Button>
            <Button className='ButtonDelete' variant='outlined' onClick={() => handleDelete(accounts.user_id)}>
              Delete
            </Button>
            </div>
          </CardContent> 
        </Card>
      ))
    }
    </>
  )
}
