import React from "react";
import "./TaskForm.css";
import axios from "axios";
import { useState, useEffect } from "react";
import {
  Button,
  Card,
  CardContent,
  CircularProgress,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import { alpha, styled } from '@mui/material/styles';
import { ColorLens, Task } from "@mui/icons-material";

export default function TaskForm() {
  const navigate = useNavigate();
  const params = useParams();

  const [loading, setLoading] = useState(false);
  const [editing, setEditing] = useState(false);
  const [form, setForm] = useState({
    username: "",
    password: "",
  });

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const submit = async (e) => {
    e.preventDefault();
    setLoading(true);

    if (editing) {
      const res = await axios.put(`http://localhost:4000/tasks/${params.id}`, {
      username: form.username,
      password: form.password,
      });

    } else {
      const res = await axios.post("http://localhost:4000/tasks", {
      method: "POST",
      username: form.username,
      password: form.password,
    });
   
    }

    setLoading(false);
    navigate("/");
  };

  const loadAccounts = async (id) => {
    const response = await axios.get(`http://localhost:4000/tasks/${id}`)
    setForm({username: response.data.username, password: response.data.password})
    setEditing(true)
  }

  useEffect(() => {
    if (params.id){
      loadAccounts(params.id)
    }

  }, [params.id])



  return (
    <Grid container className="GridTaskForm">
      <Grid item xs={3}>
        <Card className="CardTaskForm">
          <Typography className="TypographyTaskForm" variant="5">
            {editing ? 'Edit' : 'Create'}
          </Typography>
          <CardContent>
            <form onSubmit={(e) => submit(e)}>
              <TextField
                required
                className="TextFiledTaskForm1"
                variant="outlined"
                label="Write your User"
                placeholder="test"
                name="username"
                value={form.username}
                inputProps = {{style: {color: 'white', }}}
                InputLabelProps = {{style: {color: 'white'}}}
                onChange={handleChange}
              />
              <TextField
                required
                className="TextFiledTaskForm2"
                variant="outlined"
                label="Write your PassWord"
                multiline
                rows={3}
                name="password"
                value={form.password}
                inputProps = {{style: {color: 'white'}}}
                InputLabelProps = {{style: {color: 'white'}}}
                onChange={handleChange}
              />
              <Button
                className="ButtonTaskForm"
                variant="outlined"
                color="primary"
                type="submit"
                disabled={!form.username || !form.password}
              >
                {loading ? (
                  <CircularProgress color="inherit" size={24} />
                ) : (
                  "Save"
                )}{" "}
              </Button>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}
