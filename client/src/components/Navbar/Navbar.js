import React from "react";
import './Navbar.css';
import {
  Box,
  AppBar,
  Container,
  Toolbar,
  Typography,
  Button,
} from "@mui/material";
import { Link, useNavigate } from "react-router-dom";

export default function Navbar() {
  const navigate = useNavigate();
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar className="AppBarNav" >
        <Container>
          <Toolbar>
            <Typography className="TypographyNav" variant="h6" sx={{ flexGrow: 1 }}>
              <Link to="/" className="LinkNav">PERN STACK</Link>
            </Typography>
            <Button
              variant="outlined"
              color="primary"
              onClick={() => navigate("/tasks/new")}
            >
              New Task
            </Button>
          </Toolbar>
        </Container>
      </AppBar>
    </Box>
  );
}
